<div class="page-header">
    <h1>Create Post</h1>
</div>

<div>
    <form class="form-horizontal" role="form" action="/home/postExternal" method="post">
        <div class="form-group">
            <label class="col-sm-3">Posting as</label>
            <div class="col-sm-8">
                <span><?php echo $viewData["user-profile"]["name"] ?></span>
                <button class="btn btn-default btn-xs">change</button>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Posting on page</label>
            <div class="col-sm-8">
                <span><?php echo $viewData["page-profile"]["name"] ?></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Link</label>
            <div class="col-sm-8">
                <input class="form-control" name="post-link" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Mensaje</label>
            <div class="col-sm-8">
                <textarea class="form-control" name="post-message"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-3">
                <button class="btn btn-primary btn-lg">Send</button>
            </div>
        </div>
    </form>
</div>