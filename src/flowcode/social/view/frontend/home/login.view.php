<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <?php if (isset($viewData["user-profile"])): ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <span>You're already logged as: <?php echo $viewData["user-profile"]["name"] ?></span>
                    <button class="btn btn-danger btn-lg">Logout</button>
                </div>
            </div>
        <?php else: ?>
            <a class="btn btn-info btn-lg" href="<?php echo $viewData["login-url"] ?>">Please login.</a>
        <?php endif; ?>
    </div>
</div>