<div class="page-header">
    <h1>Welcome <?php echo $viewData["user-profile"]["name"] ?></h1>
</div>

<div>
    <div>
        <a class="btn btn-primary btn-lg" href="/home/createPost" >Make a Post to my Wall</a>
        <a class="btn btn-success btn-lg" href="/home/createPostExternal" >Make a Post to FanPage</a>
    </div>
</div>
