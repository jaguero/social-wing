<?php

namespace flowcode\social\controller;

use Facebook;
use FacebookApiException;
use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\View;

/**
 * Description of HomeController
 *
 * @author juanma
 */
class HomeController extends BaseController {

    private $loginConfig;
    private $appConfig;
    private $facebookApp;

    public function __construct() {
        $this->setIsSecure(FALSE);
        $this->loginConfig = null;
        $this->appConfig = null;
        $this->facebookApp = null;
    }

    public function index(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();
        if ($user_id) {
            try {
                $viewData["user-profile"] = $this->getFBApp()->api('/me', 'GET');
            } catch (FacebookApiException $e) {
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }
        return new View($viewData, "frontend/home/index");
    }

    public function login(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();

        if ($user_id) {
            $viewData["user-profile"] = $this->getFBApp()->api('/me', 'GET');
        } else {
            $viewData["login-url"] = $this->getFBApp()->getLoginUrl($this->getLoginConfig());
        }
        return new View($viewData, "frontend/home/login");
    }

    public function createPost(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();
        if ($user_id) {
            try {
                $viewData["user-profile"] = $this->getFBApp()->api('/me', 'GET');
            } catch (FacebookApiException $e) {
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }

        return new View($viewData, "frontend/post/create");
    }

    public function createPostExternal(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();
        if ($user_id) {
            try {
                $viewData["user-profile"] = $this->getFBApp()->api('/me', 'GET');
                $viewData["page-profile"] = $this->getFBApp()->api('/271096679713194', 'GET');
            } catch (FacebookApiException $e) {
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }

        return new View($viewData, "frontend/post/createExternal");
    }

    public function post(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();

        if ($user_id) {
            try {
                $ret_obj = $this->getFBApp()->api('/me/feed', 'POST', array(
                    'link' => $httpRequest->getParameter("post-link"),
                    'message' => $httpRequest->getParameter("post-message")
                ));
                $viewData["last-post-id"] = $ret_obj['id'];
            } catch (FacebookApiException $e) {
                print_r($e);
                die();
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }

        return new View($viewData, "frontend/post/postCreated");
    }

    public function postExternal(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();

        if ($user_id) {
            try {
                $ret_obj = $this->getFBApp()->api('/271096679713194/feed', 'POST', array(
                    'link' => $httpRequest->getParameter("post-link"),
                    'message' => $httpRequest->getParameter("post-message")
                ));
                $viewData["last-post-id"] = $ret_obj['id'];
            } catch (FacebookApiException $e) {
                echo $e->getCode() . ":  " . $e->getMessage();
                die();
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }

        return new View($viewData, "frontend/post/postCreated");
    }

    public function posts(HttpRequest $httpRequest) {
        $user_id = $this->getFBApp()->getUser();

        if ($user_id) {
            try {
                $ret_obj = $this->getFBApp()->api('/me/feed?limit=5', 'GET');
                $viewData["result"] = $ret_obj;
            } catch (FacebookApiException $e) {
                $this->redirect("/home/login");
            }
        } else {
            $this->redirect("/home/login");
        }

        return new View($viewData, "frontend/post/posts");
    }

    public function getLoginConfig() {
        if (is_null($this->loginConfig)) {
            $this->loginConfig = array("scope" => "publish_stream");
        }
        return $this->loginConfig;
    }

    public function getAppConfig() {
        if (is_null($this->appConfig)) {
            $this->appConfig = array(
                'appId' => '283011488514453',
                'secret' => '0cd9c1e3dd1f34ee8bed85303cf0d348',
                'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
            );
        }
        return $this->appConfig;
    }

    /**
     * Get the facebook sdk.
     * @return Facebook facebook Api isntance.
     */
    public function getFBApp() {
        if (is_null($this->facebookApp)) {
            $this->facebookApp = new Facebook($this->getAppConfig());
        }
        return $this->facebookApp;
    }

    public function setLoginConfig($loginConfig) {
        $this->loginConfig = $loginConfig;
    }

}

?>
