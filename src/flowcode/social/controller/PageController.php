<?php

namespace flowcode\social\controller;

use Facebook;
use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\PlainView;
use flowcode\enlace\view\View;

/**
 * Description of PageController
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
class PageController extends BaseController {

    public function __construct() {
        $this->setIsSecure(FALSE);
    }

    public function index(HttpRequest $httpRequest) {
        $config = array(
            'appId' => 'YOUR_APP_ID',
            'secret' => 'YOUR_APP_SECRET',
            'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
        );

        $facebook = new Facebook($config);
        $user_id = $facebook->getUser();
        
        $viewData["facebook"] = $facebook;
        $viewData["user-id"] = $user_id;

        return new View($viewData, "frontend/home/index");
    }

    public function defaultMethod(HttpRequest $httpRequest) {
        $viewData["data"] = "Default controller, default method. We strongly recommend to setup your own default controller.";
        return new PlainView($viewData);
    }

}
