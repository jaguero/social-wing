<?php

use flowcode\enlace\Enlace;

require './vendor/autoload.php';

$app = new Enlace(Enlace::$MODE_DEVELOPMENT);

/** setup */
$app->set("dir", array(
    "src" => __DIR__ . "/src",
    "log" => __DIR__ . "/log"
));
$app->set("scanneableControllers", array(
    "social" => "\\flowcode\\social\\controller\\"
));
$app->set("defaultController", "\\flowcode\\social\\controller\\PageController");
$app->set("defaultMethod", "defaultMethod");
$app->set("errorMethod", "errorMethod");

/** views layouts */
$app->set("view", array(
    "path" => __DIR__ . "/src/flowcode/social/view",
    "layout" => array(
        "frontend" => "frontend",
    )
));

/** other config */
$app->set("messages", array(
    "hello" => "hi",
));

/** routes */
$app->setRoute("homepage", array("permalink" => "home"));
$app->setRoute("home", array("controller" => "Home"));
$app->setRoute("admin", array("controller" => "AdminHome"));

/** run app */
$app->handleRequest($_SERVER['REQUEST_URI']);
?>
